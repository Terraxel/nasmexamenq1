%include "io.inc"

section .data

nbChars: dd 0
input: times 40 db 0 ;String de 31 caractères
delimiter: db '/'
nbItems: dd 0
quitState: dd 0

section .text
global CMAIN

extern _lstrlenA@4

CMAIN:
    mov ebp, esp; for correct debugging
    mov edx, 0
    
    startProgram:
    call GETSTRINGANDLENGTH
    mov edx, 1
    call GETNUMBEROFITEMS
    call DISPLAYENTRY
    
    cmp dword[quitState], 0 ;Si 0 on recommence sinon on quitte
    je startProgram
    
    xor eax, eax
    ret
    
    GETSTRINGANDLENGTH:
        
        cmp edx, 1
        jne get
        GET_CHAR ecx
        get:
        PRINT_STRING "Entrez les donnees : "
        NEWLINE
        GET_STRING input, 40
        push input
        call _lstrlenA@4
        mov [nbChars], eax
        ret
    
    GETNUMBEROFITEMS:
        
        push eax
        push ebx   
        mov eax, 1
        mov ebx, (-1)
        ;Saving previous eax and ebx values. eax will contain nbofitems and ebx is a counter
        countloop:
        inc ebx
        cmp ebx, [nbChars]
        jg endcount
        cmp byte[input + ebx], ','
        jne countloop
        inc eax
        jmp countloop
        
        endcount:
        mov dword[nbItems], eax
        pop ebx
        pop eax
        ret
        
    DISPLAYENTRY:
        
        begin:
        NEWLINE
        PRINT_STRING "Entrez le numero de l'element voulu (-1 quitte le programme, 0 recommence au debut) : "
        GET_DEC 4, eax
        
        cmp eax, (-1)
        je wantstoleave
        cmp eax, 0
        je wantstorestart
        cmp eax, 1
        jl begin
        cmp eax, [nbItems]
        jg begin
        
        mov ebx, (-1)
        mov ecx, 1
        displayloop:
        inc ebx
        cmp ebx, [nbChars]
        je begin
        cmp byte[input + ebx], ','
        jne charisnotdelimiter
        inc ecx
        jmp displayloop
        charisnotdelimiter:
        cmp ecx, eax
        jne displayloop
        PRINT_CHAR [input + ebx]
        jmp displayloop
        ;si on est dans le bon truc on affiche si pas non 
        
        
        wantstoleave:
        mov dword[quitState], 1
        ret
        
        wantstorestart:
        mov dword[quitState], 0
        ret
        
        
        
        
        
        
        
        
        