EXAMEN X86
==========

## Step 1

 - On va créer une chaîne de beaucoup de caractères :

    

```asm
input: times 40 db 0 
;Chaîne de 40 caractères 
```

- On récupère la chaîne entrée par l'utilisateur et on l'insère dans "input".

- Avec la fonction "lstrlenA" de WINAPI, on peut récupérer la longueur réelle de la chaîne ([Documentation lstrlenA](https://docs.microsoft.com/en-us/windows/desktop/api/winbase/nf-winbase-lstrlena))

## Step Optionnelle 

- On récupère le caractère "délimiteur" (pas encore fait).

## Step 2

- On récupère le nombre d'éléments de la chaîne grâce au caractère délimiteur (on a => nombre d'éléments  = nombre de caractères délimiteurs + 1).

## Step 3 

- On a le nombre d'élements, on demande donc à l'utilisateur quel élement récupérer. Il faut que cet élement soit compris entre 1 (inclus) et le nombre d'élements maximal (que l'on a récupéré en step 2).

- Enfin, on loop du premier caractère jusqu'au dernier. On affiche que les caractères de l'élement que l'on veut :

        Par exemple, Si j'ai :

        "Tomate,Kiwi,Carottes"

        Je demande le deuxième élément. Je loope, quand j'arrive à la première virgule, je sais que je suis au deuxième élément. J'affiche donc tous les caractères (avec un PRINT_CHAR) un par un jusqu'à la prochaine virgule (non incluse).

        J'aurai en output : "Kiwi"
